const notasDisponiveis = [100, 50, 20, 10];

class Cedulas {

    disnponibilizaSaque(valor, res) {
        let data = this.saque(valor, 0, {resto: 0, notas: []})
        try {
            res.status(201).json(data)
        } catch (e) {
            res.status(400).json(e)
        }
    }

    saque(valor, tentativa, resultado) {
        if (notasDisponiveis[tentativa] === undefined) {
            resultado.resto = valor;
            return resultado;
        }

        let divisao = Math.floor(valor / notasDisponiveis[tentativa]);

        if (divisao > 0) {
            valor -= divisao * notasDisponiveis[tentativa];
            resultado.notas.push({cedula: notasDisponiveis[tentativa], quantidade: divisao});
        }

        return this.saque(valor, ++tentativa, resultado);
    }
}

module.exports = new Cedulas